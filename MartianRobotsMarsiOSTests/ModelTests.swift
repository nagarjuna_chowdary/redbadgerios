//
//  ModelTests.swift
//  MartianRobotsMarsiOSTests
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import XCTest
@testable import MartianRobotsMarsiOS

class ModelTests: XCTestCase {
    func testModels() {

        let northFacing = Location(coordinate: Coordinate(x: 3, y: 3), orientation: Orientation.north)
        let eastFacing = Location(coordinate: Coordinate(x: 3, y: 3), orientation: Orientation.east)
        let southFacing = Location(coordinate: Coordinate(x: 3, y: 3), orientation: Orientation.south)
        let westFacing = Location(coordinate: Coordinate(x: 3, y: 3), orientation: Orientation.west)

        XCTAssertEqual(Command.left.execute(from: northFacing), Location(coordinate: northFacing.coordinate, orientation: .west))
        XCTAssertEqual(Command.left.execute(from: eastFacing), Location(coordinate: eastFacing.coordinate, orientation: .north))
        XCTAssertEqual(Command.left.execute(from: southFacing), Location(coordinate: southFacing.coordinate, orientation: .east))
        XCTAssertEqual(Command.left.execute(from: westFacing), Location(coordinate: westFacing.coordinate, orientation: .south))

        XCTAssertEqual(Command.right.execute(from: northFacing), Location(coordinate: northFacing.coordinate, orientation: .east))
        XCTAssertEqual(Command.right.execute(from: eastFacing), Location(coordinate: eastFacing.coordinate, orientation: .south))
        XCTAssertEqual(Command.right.execute(from: southFacing), Location(coordinate: southFacing.coordinate, orientation: .west))
        XCTAssertEqual(Command.right.execute(from: westFacing), Location(coordinate: westFacing.coordinate, orientation: .north))

        XCTAssertEqual(Command.forward.execute(from: northFacing), Location(coordinate: Coordinate(x: 3, y: 4), orientation: .north))
        XCTAssertEqual(Command.forward.execute(from: eastFacing), Location(coordinate: Coordinate(x: 4, y: 3), orientation: .east))
        XCTAssertEqual(Command.forward.execute(from: southFacing), Location(coordinate: Coordinate(x: 3, y: 2), orientation: .south))
        XCTAssertEqual(Command.forward.execute(from: westFacing), Location(coordinate: Coordinate(x: 2, y: 3), orientation: .west))
    }
}
