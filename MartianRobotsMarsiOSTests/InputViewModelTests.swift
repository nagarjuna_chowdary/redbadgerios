//
//  InputViewModelTests.swift
//  MartianRobotsMarsiOSTests
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import XCTest
@testable import MartianRobotsMarsiOS

class InputViewModelTests: XCTestCase {
    func testWithActualData() {
        let inputString = """
                            5 3
                            1 1 E
                            RFRFRFRF

                            3 2 N
                            FRRFLLFFRRFLL

                            0 3 W
                            LLFFFLFLFL
                            """
        let expectedOutputString = "1 1 E\n3 3 N LOST\n2 3 S\n"

        if let jsonString = ParserService().jsonString(from: inputString),
            let instructions = ParserService().instructions(from: jsonString),
            let grid = Grid(instructions: instructions) {

            InputViewModel().instructRobot(from: grid)
            let result = InputViewModel().string(from: grid)

            XCTAssertEqual(result, expectedOutputString)
        } else {
            XCTFail("Failed to setup with sample data")
        }
    }

    func testwithFailData() {
        let inputString = """
                            5 3
                            1 1 E
                            RFRFRFRF

                            3 2 N
                            FRRFLLFFRRFLL
                            """
        let expectedOutputString = "2 3 N \n3 3 S\n"

        if let jsonString = ParserService().jsonString(from: inputString),
            let instructions = ParserService().instructions(from: jsonString),
            let grid = Grid(instructions: instructions) {

            InputViewModel().instructRobot(from: grid)
            let result = InputViewModel().string(from: grid)

            XCTAssertNotEqual(result, expectedOutputString)
        } else {
            XCTFail("Failed to setup with single lost data")
        }
    }
}
