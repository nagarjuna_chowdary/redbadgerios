//
//  InputViewModel.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

protocol InputViewModelDelegate {
    func printRobotSteps()
}
final class InputViewModel {

    public func instructRobot(from grid: Grid) {
        for (robotIndex, robot) in grid.instructions.robots.enumerated() {
            var preFinalMovelocation = robot.location
            var location = robot.location
            var finalCommand: Command?

            for command in robot.commands {
                preFinalMovelocation = location
                if isSafeToMove(location: location, command: command, grid: grid) {

                    finalCommand = command
                    location = command.execute(from: location)

                    let isLost = self.isRobotLost(location: location, grid: grid)
                    if isLost {
                        location = preFinalMovelocation
                        location.coordinate.markAsLost()
                        break
                    } else {
                        print("Robot: \(robotIndex) Orientation: \(location.orientation.rawValue) Location \(location.coordinate.x, location.coordinate.y)")
                    }
                } else {
                    print("Robot: \(robotIndex) Command: \(command.rawValue) Orientation: \(location.orientation.rawValue) Location: \(location.coordinate.x, location.coordinate.y)")
                }
            }

            let postMoves = Robot(location: location, commands: robot.commands)
            if postMoves.isLost, let cmd = finalCommand {
                grid.add(lost: postMoves, command: cmd)
            } else {
                grid.add(result: postMoves)
            }
        }
    }

    private func isRobotLost(location: Location, grid: Grid) -> Bool {
        if location.coordinate.x >= 0,
            location.coordinate.x < grid.instructions.width,
            location.coordinate.y >= 0,
            location.coordinate.y < grid.instructions.height {
            return false
        } else {
            return true
        }
    }

    private func isSafeToMove(location: Location, command: Command, grid: Grid) -> Bool {
        return !grid.items[location.coordinate.x][location.coordinate.y].isCommand(command: command, orientation: location.orientation)
    }

    public func string(from grid: Grid) -> String {

        var formatted = ""

        grid.results.forEach({ robot in
            formatted += "\(status(robot: robot))\n"
        })

        return formatted
    }

    private func status(robot: Robot) -> String {
        let x = robot.location.coordinate.x
        let y = robot.location.coordinate.y
        let o = robot.location.orientation.rawValue
        let lostStatus = robot.location.coordinate.isLost ? " LOST" : ""

        return "\(x) \(y) \(o)\(lostStatus)"
    }
}

