//
//  InputViewController.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import UIKit

class InputViewController: UIViewController {
    @IBOutlet weak var inputTextView: UITextView!

    @IBOutlet weak var outputTextView: UITextView!
    lazy var viewModel: InputViewModel = {
        return InputViewModel()
    }()

    override func viewDidLoad() {
        inputTextView.text = InputConstants.input
        if let jsonString = ParserService().jsonString(from: InputConstants.input),
            let instructions = ParserService().instructions(from: jsonString),
            let grid = Grid(instructions: instructions) {

            viewModel.instructRobot(from: grid)
            let result = viewModel.string(from: grid)
            print(result)
            outputTextView.text = result
        }
    }
}
