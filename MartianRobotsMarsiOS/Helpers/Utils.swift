//
//  Utils.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

class Utils {

    func isValid(instructions: Instructions) -> Bool {
        guard instructions.width > Constants.gridMinX,
            instructions.width <= (Constants.gridMaxX + 1),
            instructions.height > Constants.gridMinY,
            instructions.height <= (Constants.gridMaxY + 1),
            instructions.robots.count > 0,
            isAllRobotsWithinBounds(instructions: instructions) else {
                return false
        }

        return true
    }

    private func isAllRobotsWithinBounds(instructions: Instructions) -> Bool {

        let strayRobotCount = instructions.robots
            .filter({
                $0.location.coordinate.x < 0 ||
                    $0.location.coordinate.x >= instructions.width ||
                    $0.location.coordinate.y < 0 ||
                    $0.location.coordinate.y >= instructions.height })
            .count

        return strayRobotCount == 0
    }
}
