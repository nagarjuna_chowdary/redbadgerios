//
//  Constants.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

struct Constants {
    static let gridMinX = 0
    static let gridMaxX = 50
    static let gridMinY = 0
    static let gridMaxY = 50
}

struct InputConstants {
    static let input = """
                        5 3
                        1 1 E
                        RFRFRFRF
                        3 2 N
                        FRRFLLFFRRFLL
                        0 3 W
                        LLFFFLFLFL
                       """
}
