//
//  ParserService.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

class ParserService {

    func instructions(from jsonString: String) -> Instructions? {
        guard let data = jsonString.data(using: .utf8) else { return nil }

        return try? JSONDecoder().decode(Instructions.self, from: data)
    }

    func jsonString(from rawText: String) -> String? {

        let lines = rawText
            .replacingOccurrences(of: "\r", with: "\n")
            .split(separator: "\n")
            .map({ String($0).trimmingCharacters(in: .whitespaces) })
            .filter { !$0.isEmpty }

        if lines.isEmpty {
            return nil
        } else {
            var jsonLines: [String] = []

            jsonLines.append("{")

            if let gridSize = parseGridSize(from: lines[0]) {
                jsonLines.append(gridSize)

                if lines.count > 1 {
                    jsonLines.append(", \"robots\": [")
                    jsonLines.append(contentsOf: parseRobots(from: lines))
                    jsonLines.append("]")
                }
            }

            jsonLines.append("}")
            let jsonString = jsonLines.joined()
            return jsonString
        }
    }

    func parseGridSize(from str: String) -> String? {
        guard let regex = try? NSRegularExpression(pattern: "\\S+") else { return nil }

        let results = regex.matches(in: str, range: NSRange(str.startIndex..., in: str))
        let fields = results.map { String(str[Range($0.range, in: str)!]) }

        guard fields.count == 2,
            let width = Int(fields[0]),
            let height = Int(fields[1]) else {
                return nil
        }

        return "\"width\": \(width+1), \"height\": \(height+1)"
    }

    func parseRobots(from lines: [String]) -> [String] {
        guard lines.count > 1 else { return [] }
        guard (lines.count % 2) == 1 else { return [] }

        var jsonLines: [String] = []

        for idx in stride(from: 1, to: lines.count, by: 2) {
            if let robotPos = parseRobotPosition(from: lines[idx]),
                let robotCommands = parseRobotCommands(from: lines[idx+1]) {
                jsonLines.append("{ \"location\": \(robotPos), \"commands\": \(robotCommands) }, ")
            }
        }

        // trim ", " off end
        jsonLines[jsonLines.count-1] = jsonLines.last!.replacingOccurrences(of: ", ",
                                                                            with: "",
                                                                            options: [.backwards, .anchored],
                                                                            range: nil)

        return jsonLines
    }

    func parseRobotPosition(from str: String) -> String? {
        guard let regex = try? NSRegularExpression(pattern: "\\S+") else { return nil }

        let results = regex.matches(in: str, range: NSRange(str.startIndex..., in: str))
        let fields = results.map { String(str[Range($0.range, in: str)!]) }

        guard fields.count == 3,
            let x = Int(fields[0]),
            let y = Int(fields[1]),
            let orientation = Orientation(rawValue: fields[2]) else {
                return nil
        }

        return "{ \"coordinate\": { \"x\": \(x), \"y\": \(y), \"isLost\": false }, \"orientation\": \"\(orientation.rawValue)\" }"
    }

    func parseRobotCommands(from str: String) -> String? {
        guard let regex = try? NSRegularExpression(pattern: "\\S") else { return nil }

        let results = regex.matches(in: str, range: NSRange(str.startIndex..., in: str))
        let fields = results.map { String(str[Range($0.range, in: str)!]) }

        guard fields.count > 0 else { return nil }

        var result = "["
        fields.forEach({
            let cmd = Command(rawValue: $0)?.rawValue ?? Command.unknown.rawValue
            result.append("\"\(cmd)\", ")
        })

        // trim ", " off end
        result = result.replacingOccurrences(of: ", ",
                                             with: "",
                                             options: [.backwards, .anchored],
                                             range: nil)

        result.append("]")
        return result
    }
}
