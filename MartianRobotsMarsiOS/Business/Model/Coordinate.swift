//
//  Coordinate.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

struct Coordinate: Codable, Equatable {

    let x: Int
    let y: Int
    private(set) var isLost: Bool

    init(x: Int, y: Int, isLost: Bool = false) {
        self.x = x
        self.y = y
        self.isLost = isLost
    }

    mutating func markAsLost() {
        isLost = true
    }
}
