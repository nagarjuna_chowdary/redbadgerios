//
//  Orientation.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

enum Orientation: String, Equatable, Codable {

    case north = "N"
    case east = "E"
    case south = "S"
    case west = "W"

    var fullString: String {
        switch self {
        case .north: return "North"
        case .east: return "East"
        case .south: return "South"
        case .west: return "West"
        }
    }
}
