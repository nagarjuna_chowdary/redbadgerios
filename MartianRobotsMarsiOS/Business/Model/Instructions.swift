//
//  Instructions.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

struct Instructions: Codable {

    let width: Int
    let height: Int
    let robots: [Robot]

    static func empty() -> Instructions {
        return Instructions(width: Constants.gridMinX, height: Constants.gridMinY, robots: [])
    }
}
