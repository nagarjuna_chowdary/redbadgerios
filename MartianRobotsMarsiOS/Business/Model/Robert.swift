//
//  Robert.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

struct Robot: Codable {

    let location: Location
    let commands: [Command]

    var isLost: Bool {
        return location.coordinate.isLost
    }
}
