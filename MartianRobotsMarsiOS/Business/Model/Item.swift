//
//  Item.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

class Item {

    private(set) var robots: [Robot] = []
    private(set) var lostRobots: [Robot] = []
    private var commands: [(Command, Orientation)] = []

    func add(robot: Robot) {
        robots.append(robot)
    }

    func add(lost robot: Robot, command: Command) {
        lostRobots.append(robot)
        commands.append((command, robot.location.orientation))
    }

    func isCommand(command: Command, orientation: Orientation) -> Bool {
        return commands
            .filter({ $0.0 == command && $0.1 == orientation })
            .count > 0
    }
}
