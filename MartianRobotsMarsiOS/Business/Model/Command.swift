//
//  Command.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

enum Command: String {

    case left = "L"
    case right = "R"
    case forward = "F"
    case unknown = "?"

    func execute(from location: Location) -> Location {
        guard !location.coordinate.isLost else { return location }

        switch (self, location.orientation) {
        case (.left, .north):
            return Location(coordinate: location.coordinate, orientation: Orientation.west)
        case (.left, .east):
            return Location(coordinate: location.coordinate, orientation: Orientation.north)
        case (.left, .south):
            return Location(coordinate: location.coordinate, orientation: Orientation.east)
        case (.left, .west):
            return Location(coordinate: location.coordinate, orientation: Orientation.south)
        case (.right, .north):
            return Location(coordinate: location.coordinate, orientation: Orientation.east)
        case (.right, .east):
            return Location(coordinate: location.coordinate, orientation: Orientation.south)
        case (.right, .south):
            return Location(coordinate: location.coordinate, orientation: Orientation.west)
        case (.right, .west):
            return Location(coordinate: location.coordinate, orientation: Orientation.north)
        case (.forward, .north):
            return Location(coordinate: Coordinate(x: location.coordinate.x, y: location.coordinate.y + 1), orientation: location.orientation)
        case (.forward, .east):
            return Location(coordinate: Coordinate(x: location.coordinate.x + 1, y: location.coordinate.y), orientation: location.orientation)
        case (.forward, .south):
            return Location(coordinate: Coordinate(x: location.coordinate.x, y: location.coordinate.y - 1), orientation: location.orientation)
        case (.forward, .west):
            return Location(coordinate: Coordinate(x: location.coordinate.x - 1, y: location.coordinate.y), orientation: location.orientation)
        default:
            return Location(coordinate: location.coordinate, orientation: location.orientation)
        }
    }
}

extension Command: Codable {

    init(from decoder: Decoder) throws {
        let str = try decoder.singleValueContainer().decode(String.self)
        self = Command(rawValue: str) ?? .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        try container.encode(self.rawValue)
    }
}
