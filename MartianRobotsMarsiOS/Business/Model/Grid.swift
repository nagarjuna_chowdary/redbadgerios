//
//  Grid.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

class Grid {

    let instructions: Instructions
    private(set) var results: [Robot] = []
    private(set) var items: [[Item]] = []

    init?(instructions: Instructions) {
        guard Utils().isValid(instructions: instructions) else { return nil }

        self.instructions = instructions
        for x in stride(from: 0, to: instructions.width, by: 1) {
            self.items.append([])
            for _ in stride(from: 0, to: instructions.height, by: 1) {
                self.items[x].append(Item())
            }
        }
    }

    func add(result robot: Robot) {
        results.append(robot)
        items[robot.location.coordinate.x][robot.location.coordinate.y].add(robot: robot)
    }

    func add(lost robot: Robot, command: Command) {
        results.append(robot)
        items[robot.location.coordinate.x][robot.location.coordinate.y].add(lost: robot, command: command)
    }
}
