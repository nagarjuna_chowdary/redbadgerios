//
//  Location.swift
//  MartianRobotsMarsiOS
//
//  Created by Nagarjuna on 10/01/2019.
//  Copyright © 2019 Universe. All rights reserved.
//

import Foundation

struct Location: Codable, Equatable {

    var coordinate: Coordinate
    let orientation: Orientation
}
